<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RecordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => 'required',
            'description' => 'required',
            'code' => 'required'
        ];
    }

    /**
     * Custom messages
     *
     * @return string[]
     */
    public function messages(): array
    {
        return [
          'name.required' => 'Name field is required',
          'description.required' => 'Description field is required',
          'code.required' => 'Code field is required'
        ];
    }
}
