<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AuthController extends Controller
{
    /**
     * User Login
     * @param Request $request
     * @return JsonResponse
     */
    public function login(Request $request): JsonResponse
    {
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            $user = auth()->user();
            return response()->json([
                'token' => $user->createToken('sanctum-token')->plainTextToken,
                'message' => 'Logged in successfully.'
            ]);
        }
        return response()->json([
            'message' => 'Unauthorized'
        ]);
    }

    /**
     * @return JsonResponse
     */
    public function logout(): JsonResponse
    {
        Auth::logout();
        return response()->json([
            'message' => 'Logged out successfully'
        ]);
    }

    /**
     * Get Auth User
     * @return JsonResponse
     */
    public function authUser(): JsonResponse
    {
        return response()->json([
            'data' => UserResource::make(auth()->user())
        ]);
    }
}
