<?php

namespace App\Http\Controllers;

use App\Http\Requests\RecordRequest;
use App\Http\Resources\RecordResource;
use App\Models\Record;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class RecordController extends Controller
{
    /**
     * Listing of the resource.
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $records = new Record();
        if ($request->has('status')) {
            switch ($request->status) {
                case 'inactive':
                    $records = $records->whereStatus(false);
                    break;
                case 'active':
                    $records = $records->whereStatus(true);
                    break;
            }
        }
        $records = $records->orderBy('created_at', 'desc')->get();
        return response()->json([
            'data' => RecordResource::collection($records)
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param RecordRequest $request
     * @return JsonResponse
     */
    public function store(RecordRequest $request): JsonResponse
    {
        try {
            DB::beginTransaction();
            $record = Record::create([
                'uuid' => Str::uuid()->toString(),
                'name' => $request->name,
                'description' => $request->description,
                'code' => $request->code,
                'status' => $request->status
            ]);
            DB::commit();
            return response()->json([
                'data' => RecordResource::make($record),
                'message' => 'Record created successfully'
            ]);
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json([
                'message' => $exception->getMessage()
            ]);
        }
    }

    /**
     * Display the specified resource.
     * @param $uuid
     * @return JsonResponse
     */
    public function show($uuid): JsonResponse
    {
        return response()->json([
            'data' => RecordResource::make(Record::whereUuid($uuid)->first())
        ]);
    }

    /**
     * Update the specified resource in storage.
     * @param RecordRequest $request
     * @param $uuid
     * @return JsonResponse
     */
    public function update(RecordRequest $request, $uuid): JsonResponse
    {
        try {
            DB::beginTransaction();
            $record = Record::whereUuid($uuid)->first();
            $record->name = $request->name;
            $record->description = $request->description;
            $record->code = $request->code;
            $record->status = $request->status;
            $record->save();
            DB::commit();
            return response()->json([
                'message' => 'Record updated successfully.'
            ]);
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json([
                'message' => $exception->getMessage()
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param Record $record
     * @return JsonResponse
     */
    public function destroy(Record $record): JsonResponse
    {
        try {
            DB::beginTransaction();
            $record->delete();
            DB::commit();
            return response()->json([
                'message' => 'Record deleted successfully.'
            ]);
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json([
                'message' => $exception->getMessage()
            ]);
        }
    }

    /**
     * Activate resource
     * @param Record $record
     * @return JsonResponse
     */
    public function activate(Record $record): JsonResponse
    {
        try {
            DB::beginTransaction();
            $record->update(['status' => true]);
            DB::commit();
            return response()->json([
                'message' => 'Record activated successfully'
            ]);
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json([
               'message' => $exception->getMessage()
            ]);
        }
    }
}
