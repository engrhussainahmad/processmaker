import {createStore} from 'vuex'
import axios from 'axios'
import router from '../router'

const store = createStore({
    state() {
        return {
            loggedIn: false,
            authUser: {},
            records: []
        }
    },
    getters: {
        loggedIn: state => {
            return state.loggedIn
        },
        authUser: state => {
            return state.authUser
        },
        records: state => {
            return state.records
        }
    },
    mutations: {
        setLoggedIn: (state, payload) => {
            state.loggedIn = payload
        },
        setAuthUser: (state, payload) => {
            state.authUser = payload
        },
        setRecords: (state, payload) => {
            state.records = payload
        }
    },
    actions: {
        getAuthUser: ({commit}) => {
            return axios.get('/api/user').then(({data}) => {
                commit('setAuthUser', data)
                commit('setLoggedIn', true)
                router.push({name: 'record'})
            }).catch(_ => {
                commit('setAuthUser', {})
                commit('setLoggedIn', false)
            })
        },
        getRecords: ({commit}, status) => {
            axios.get(`/api/records?status=${status}`).then(({data}) => {
                commit('setRecords', data.data)
            })
        }
    }
})

export default store
