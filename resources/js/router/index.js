import {createRouter, createWebHistory} from 'vue-router'
import store from '../store'

import Login from '../pages/Login'
import Record from '../pages/record/Record'
import AddRecord from '../pages/record/AddRecord'
import EditRecord from '../pages/record/EditRecord'

const routes = [
    {
        name: 'login',
        path: '/login',
        component: Login,
        meta: {
            middleware: 'guest',
            title: `Login`
        }
    },
    {
        name: 'record',
        path: '/',
        component: Record,
        meta: {
            middleware: 'auth',
            title: `Records`
        }
    },
    {
        name: 'add-record',
        path: '/records/add',
        component: AddRecord,
        meta: {
            middleware: 'auth',
            title: `Add Record`
        }
    },
    {
        name: 'edit-record',
        path: '/records/edit/:uuid',
        component: EditRecord,
        meta: {
            middleware: 'auth',
            title: `Edit Record`
        }
    },
]

let router = createRouter({
    mode: 'history',
    history: createWebHistory(),
    routes
})

router.beforeEach((to, from, next) => {
    document.title = `${process.env.MIX_APP_NAME} | ${to.meta.title}`
    const token = localStorage.getItem('token')
    if (to.meta.middleware === 'guest') {
        if (store.state.loggedIn || token) {
            next({name: 'record'})
        }
        next()
    } else {
        if (store.state.loggedIn || token) {
            next()
        } else {
            next({name: 'login'})
        }
    }
})

export default router
