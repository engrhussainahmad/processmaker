<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\RecordController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', [AuthController::class, 'login']);

Route::group(['middleware' => 'auth:sanctum'], function () {
   Route::get('user', [AuthController::class, 'authUser']) ;
   Route::post('logout', [AuthController::class, 'logout']);

   Route::get('records', [RecordController::class, 'index']);
   Route::post('records', [RecordController::class, 'store']);
   Route::get('records/{uuid}', [RecordController::class, 'show']);
   Route::put('records/{uuid}', [RecordController::class, 'update']);
   Route::delete('records/{record}', [RecordController::class, 'destroy']);
   Route::put('records/activate/{record}', [RecordController::class, 'activate']);
});
